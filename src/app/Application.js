import config from "../config";
import EventEmitter from "eventemitter3";
// const fetch = require("node-fetch");

const EVENTS = {
  APP_READY: "app_ready",
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
  constructor() {
    super();

    this.config = config;
    this.data = {};

    this.init();
  }

  async fetchAllPlanets() {
    let dataForAllPlanets = [];
    const response = await fetch("https://swapi.booost.bg/api/planets/");
    const planets = await response.json();
    planets.results.forEach((e) => {
      dataForAllPlanets.push(e);
    });
    for (let i = 1; i <= 5; i++) {
      const response = await fetch(`https://swapi.booost.bg/api/planets/?page=${i}`);
      const planets = await response.json();
      planets.results.forEach((e) => {
      dataForAllPlanets.push(e);
      });

    }

    return {
      planets: dataForAllPlanets,
      count: dataForAllPlanets.length
    };
  }
  static get events() {
    return EVENTS;
  }

  /**
   * Initializes the app.
   * Called when the DOM has loaded. You can initiate your custom classes here
   * and manipulate the DOM tree. Task data should be assigned to Application.data.
   * The APP_READY event should be emitted at the end of this method.
   */
  async init() {
    // Initiate classes and wait for async operations here.
    this.fetchAllPlanets().then((data) => {
      this.data = data;
      console.log(data);
      this.emit(Application.events.APP_READY);
    });
  }
}
